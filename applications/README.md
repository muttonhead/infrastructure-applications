# Applications

## Summary

This is where your kustomization to deploy [podinfo](https://github.com/stefanprodan/podinfo) will go.

## Directories

### podinfo

This directory contains a single deployment with the other resources it may need
- Deployment
- HoriozonalPodAutoscaler
- Service
- Ingress

### overlays

This directory contains a single overlay for a dev environment, it will ensure each resource deployed has a label
of `environment=dev` and updates the podinfo container to a newer version.

## Deployment

### Pre-requisites

This deployment expects the nginx ingress controller to be installed and ready, this can be accomplished by applying the manifests
as shown below. Note: ideally this would get pulled in via helm / kustomize / etc and be applied automatically for each environment,
but was installed this way to keep this demo simple.

```
kubectl apply -f https://raw.githubusercontent.com/kubernetes/ingress-nginx/controller-v1.10.0/deploy/static/provider/cloud/deploy.yaml
```

### podinfo

To deploy the podinfo application ensure `kubectl` is configured for your cluster and run `kubectl apply -k overlays/dev`.
This will result in the pod running with an nginx ingress.

### testing

This was tested against a local kubernetes cluster, running a curl against the newly created ingress gives the response from
the podinfo deployment and should look similar to the contents below.

```
❯ curl http://localhost:80
{
  "hostname": "podinfo-5df66ffcc7-w9tvf",
  "version": "6.6.1",
  "revision": "d042732a4459b09915e64edb1ca804be2f8074af",
  "color": "#34577c",
  "logo": "https://raw.githubusercontent.com/stefanprodan/podinfo/gh-pages/cuddle_clap.gif",
  "message": "greetings from podinfo v6.6.1",
  "goos": "linux",
  "goarch": "arm64",
  "runtime": "go1.22.1",
  "num_goroutine": "8",
  "num_cpu": "8"
}
```
