# Infrastructure

## Summary

This is where your infrastructure terraform will go.

## Modules

This project contains a single simple module for configuring a VPC and GKE cluster, documentation for the module and its inputs
can be found [here](./module/README.md). As it is a simple example the amount of inputs / customizations are limited.

## Deployment

This repository has a simple one file terragrunt configuration to handle the remote state and deployment. To deploy / apply
changes run `terragrunt apply` in this `infrastructure` directory or `terragrunt plan` to see what will change. This expects
an environment variable `GOOGLE_APPLICATION_CREDENTIALS=$HOME/.config/gcloud/application_default_credentials.json` to be set
or pointed to the correct file with your gcloud credentials.
