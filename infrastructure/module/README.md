# Module

A basic terraform module to deploy a simple VPC and GKE cluster in GCP.

## Requirements

| Name | Version |
|------|---------|
| <a name="requirement_google"></a> [google](#requirement\_google) | 5.23.0 |
| <a name="requirement_kubernetes"></a> [kubernetes](#requirement\_kubernetes) | 2.27.0 |

## Providers

| Name | Version |
|------|---------|
| <a name="provider_google"></a> [google](#provider\_google) | 5.23.0 |

## Modules

| Name | Source | Version |
|------|--------|---------|
| <a name="module_gke"></a> [gke](#module\_gke) | terraform-google-modules/kubernetes-engine/google | 30.2.0 |

## Resources

| Name | Type |
|------|------|
| [google_compute_network.vpc](https://registry.terraform.io/providers/hashicorp/google/5.23.0/docs/resources/compute_network) | resource |
| [google_compute_subnetwork.subnet](https://registry.terraform.io/providers/hashicorp/google/5.23.0/docs/resources/compute_subnetwork) | resource |
| [google_client_config.default](https://registry.terraform.io/providers/hashicorp/google/5.23.0/docs/data-sources/client_config) | data source |

## Inputs

| Name | Description | Type | Default | Required |
|------|-------------|------|---------|:--------:|
| <a name="input_gcp_project"></a> [gcp\_project](#input\_gcp\_project) | GCP project to deploy to | `any` | n/a | yes |
| <a name="input_gcp_region"></a> [gcp\_region](#input\_gcp\_region) | GCP cloud region to deploy to | `any` | n/a | yes |
| <a name="input_gcp_zones"></a> [gcp\_zones](#input\_gcp\_zones) | Network zones to use for the GKE cluster | `any` | n/a | yes |
| <a name="input_gke_node_locations"></a> [gke\_node\_locations](#input\_gke\_node\_locations) | Locations to use for the node pool | `any` | n/a | yes |
| <a name="input_gke_node_machine_type"></a> [gke\_node\_machine\_type](#input\_gke\_node\_machine\_type) | Machine type to use for the node pool | `any` | n/a | yes |

## Outputs

No outputs.
