terraform {
  required_providers {
    google = {
      source = "hashicorp/google"
      version = "5.23.0"
    }

    kubernetes = {
      source = "hashicorp/kubernetes"
      version = "2.27.0"
    }
  }
}

provider "google" {
  project = var.gcp_project
  region = var.gcp_region
}

provider "kubernetes" {
  host                   = "https://${module.gke.endpoint}"
  token                  = data.google_client_config.default.access_token
  cluster_ca_certificate = base64decode(module.gke.ca_certificate)
}
