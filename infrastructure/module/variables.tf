# gcp config
variable "gcp_project" {
  description = "GCP project to deploy to"
}

variable "gcp_region" {
  description = "GCP cloud region to deploy to"
}

# gke config
variable "gcp_zones" {
  description = "Network zones to use for the GKE cluster"
}

variable "gke_node_machine_type" {
  description = "Machine type to use for the node pool"
}

variable "gke_node_locations" {
  description = "Locations to use for the node pool"
}
