remote_state {
  backend = "gcs"
  generate = {
    path      = "backend.tf"
    if_exists = "overwrite_terragrunt"
  }

  config = {
    project     = "gold-setup-419318"
    location    = "us-central1"
    bucket      = "gold-setup-419318-terraform-state"
    prefix      = "${path_relative_to_include()}"
    credentials = get_env("GOOGLE_APPLICATION_CREDENTIALS")
  }
}

terraform {
  source = "./module"
}


inputs = {
  gcp_project = "gold-setup-419318"
  gcp_region  = "us-central1"
  gcp_zones   = ["us-central1-a", "us-central1-b", "us-central1-f"]

  gke_node_machine_type = "e2-medium"
  gke_node_locations    = "us-central1-a,us-central1-b"
}
